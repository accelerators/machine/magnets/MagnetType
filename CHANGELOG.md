# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [11.0.0] - 20-12-2019

### Added
* A new attribute *RequireCycling* boolean / spectrum / Read

## [10.0.1] - 13-12-2019

## Added
* A new attribute *StrengthsReadPoint* to get the strengths read
point

## [9.1.0] - 06-12-2019

## Fixed
* Take Nan value into account in the atts which compute mean,  min,
max and std
* Att StrengthCorrectionOffset and StrengthCorrectionCoeff now
have a default value for their set_value
* Better error management for the Isfrozen attribute

## [9.0.3] - 03-12-2019

### Fixed
* Att Strengths and TotalCorrectionStrengths value taken
fomr the set_value of low level devices

## [9.0.2] - 28-11-2019

### Fixed
* The DesignStrengthFile att read value wwas not initialized until
it was written!

## [9.0.1] - 19-11-2019

### Fixed
* Fix bug in magnet states for the two qf2 used in injection cells

## [9.0.0] - 31-10-2019

### Added
* A new command *ResetResonaceStrengths* (void-void)

### Changed
* we now have two families for the QF4 magnets (qf4ae and qf4bd)

## [8.0.0] - 14-10-2019

### Added 
* New attribute IsFrozen (Spectrum DevBoolean R/W)

### Changed
* Attribute DisabledMagnets now built from new att IsFrozen

## [7.1.0] - 10-10-2019

### Changed
* Attribute DesignStrengthFile is still memorized but not applied
at init
* It's now possible to reead att DesignStrengthFile when the device
state is FAULT or UNKNOWN

## [7.0.0] - 04-07-2019

### Changed
* The attribute DisabledMagnets is now named FrozenMagnets
* Sub-device(s) state ALARM and OFF have a higher priority than the
DISABLE state

## [6.2.0] - 29-04-1019

### Added
* limit reading of the MagnetStates attribute to once per second. This is
done in order to poll the att DisabledMagnets (which is using the MagnetStates).
Polling of the DisabledMagnets att is required in order to have one
event when one magnet is enabled/disabled

## [6.1.0] - 20-03-2019

### Changed
* The two QF1 and QD3 injection families have been removed. Their
devices are now part of the classical QF1 and Qd3 families

## [6.0.0] - 19-03-2019

### Added
* A new spectrum attribute *DisabledMagnets* which is the list of 
disabled magnet names

## [5.1.0] - 01-02-2019

### Changed
* Change how the device state is computed. Disable state is now taken
into account. Name of all devices in "special" state are reported
in status

## [5.0.0] - 29-01-2019

### Removed
* Commands ON and OFF removed

### Added
* Attribute CorrectionStrengthsSteps. When this att is different than
1, set Correction strengths in step!!

## [4.0.0] - 22-01-2019

### Added
* Add attributes Strengths (Read - Spectrum) and TotalCorrectionStrengths 
(Read - Spectrum)
* Add four attributes xxxMSMM for Mean, Std, Min and Max
(Read - Spectrum - Double)

## [3.0.0] - 15-01-2019

### Added

* A MagnetStates attribute to retrieve all low-level devices state

## [2.3.1] - 18-12-2018

### Changed
* The Reset command executes Reset on all its sub-devices but hide
the "not allowed in this state" error

## [2.3.0] - 09-07-2018

### Added

Two new attributes:

* StrengthCorrectionOffset
* StrengthCorrectionCoeff

A Cmakelist.txt file

### Removed

The good old Makefile

## [2.2.0] - 07-06-2018

Remove the "Device name[xx] = " string part in the MagnetNames attribute

## [2.1.0] - 06-06-2018

Keep only 3 injection families for quadrupole qf1, qf2 and qd3.
Those families have only 2 elements installed in cell 04 a and cell 03 e

## [2.0.0] - 30-05-2018

### Added
* README and CHANGELOG official ESRF files
