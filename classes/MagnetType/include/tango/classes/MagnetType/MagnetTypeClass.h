/*----- PROTECTED REGION ID(MagnetTypeClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        MagnetTypeClass.h
//
// description : Include for the MagnetType root class.
//               This class is the singleton class for
//                the MagnetType device class.
//               It contains all properties and methods which the 
//               MagnetType requires only once e.g. the commands.
//
// project :     MagnetTypePS
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2009,2010,2011,2012,2013,2014,2015
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef MagnetTypeClass_H
#define MagnetTypeClass_H

/*----- PROTECTED REGION END -----*/	//	MagnetTypeClass.h

#include <tango/tango.h>
#include <tango/classes/MagnetType/MagnetType.h>



namespace MagnetType_ns
{
/*----- PROTECTED REGION ID(MagnetTypeClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	MagnetTypeClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute DesignStrengthFile class definition
class DesignStrengthFileAttrib: public Tango::Attr
{
public:
	DesignStrengthFileAttrib():Attr("DesignStrengthFile",
			Tango::DEV_STRING, Tango::READ_WRITE) {}
	~DesignStrengthFileAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_DesignStrengthFile(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<MagnetType *>(dev))->write_DesignStrengthFile(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_DesignStrengthFile_allowed(ty);}
};

//	Attribute CorrectionStrengthsSteps class definition
class CorrectionStrengthsStepsAttrib: public Tango::Attr
{
public:
	CorrectionStrengthsStepsAttrib():Attr("CorrectionStrengthsSteps",
			Tango::DEV_LONG, Tango::READ_WRITE) {}
	~CorrectionStrengthsStepsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_CorrectionStrengthsSteps(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<MagnetType *>(dev))->write_CorrectionStrengthsSteps(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_CorrectionStrengthsSteps_allowed(ty);}
};

//	Attribute Strengths class definition
class StrengthsAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for Strengths attribute
	constexpr static long X_DATA_SIZE = 610;
	StrengthsAttrib():SpectrumAttr("Strengths",
			Tango::DEV_DOUBLE, Tango::READ, StrengthsAttrib::X_DATA_SIZE) {}
	~StrengthsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_Strengths(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_Strengths_allowed(ty);}
};

//	Attribute StrengthsMSMM class definition
class StrengthsMSMMAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for StrengthsMSMM attribute
	constexpr static long X_DATA_SIZE = 4;
	StrengthsMSMMAttrib():SpectrumAttr("StrengthsMSMM",
			Tango::DEV_DOUBLE, Tango::READ, StrengthsMSMMAttrib::X_DATA_SIZE) {}
	~StrengthsMSMMAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_StrengthsMSMM(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_StrengthsMSMM_allowed(ty);}
};

//	Attribute StrengthsReadPoint class definition
class StrengthsReadPointAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for StrengthsReadPoint attribute
	constexpr static long X_DATA_SIZE = 610;
	StrengthsReadPointAttrib():SpectrumAttr("StrengthsReadPoint",
			Tango::DEV_DOUBLE, Tango::READ, StrengthsReadPointAttrib::X_DATA_SIZE) {}
	~StrengthsReadPointAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_StrengthsReadPoint(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_StrengthsReadPoint_allowed(ty);}
};

//	Attribute TotalCorrectionStrengths class definition
class TotalCorrectionStrengthsAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for TotalCorrectionStrengths attribute
	constexpr static long X_DATA_SIZE = 610;
	TotalCorrectionStrengthsAttrib():SpectrumAttr("TotalCorrectionStrengths",
			Tango::DEV_DOUBLE, Tango::READ, TotalCorrectionStrengthsAttrib::X_DATA_SIZE) {}
	~TotalCorrectionStrengthsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_TotalCorrectionStrengths(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_TotalCorrectionStrengths_allowed(ty);}
};

//	Attribute TotalCorrectionStrengthsMSMM class definition
class TotalCorrectionStrengthsMSMMAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for TotalCorrectionStrengthsMSMM attribute
	constexpr static long X_DATA_SIZE = 4;
	TotalCorrectionStrengthsMSMMAttrib():SpectrumAttr("TotalCorrectionStrengthsMSMM",
			Tango::DEV_DOUBLE, Tango::READ, TotalCorrectionStrengthsMSMMAttrib::X_DATA_SIZE) {}
	~TotalCorrectionStrengthsMSMMAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_TotalCorrectionStrengthsMSMM(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_TotalCorrectionStrengthsMSMM_allowed(ty);}
};

//	Attribute CorrectionStrengths class definition
class CorrectionStrengthsAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for CorrectionStrengths attribute
	constexpr static long X_DATA_SIZE = 610;
	CorrectionStrengthsAttrib():SpectrumAttr("CorrectionStrengths",
			Tango::DEV_DOUBLE, Tango::READ_WRITE, CorrectionStrengthsAttrib::X_DATA_SIZE) {}
	~CorrectionStrengthsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_CorrectionStrengths(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<MagnetType *>(dev))->write_CorrectionStrengths(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_CorrectionStrengths_allowed(ty);}
};

//	Attribute CorrectionStrengthsMSMM class definition
class CorrectionStrengthsMSMMAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for CorrectionStrengthsMSMM attribute
	constexpr static long X_DATA_SIZE = 4;
	CorrectionStrengthsMSMMAttrib():SpectrumAttr("CorrectionStrengthsMSMM",
			Tango::DEV_DOUBLE, Tango::READ, CorrectionStrengthsMSMMAttrib::X_DATA_SIZE) {}
	~CorrectionStrengthsMSMMAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_CorrectionStrengthsMSMM(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_CorrectionStrengthsMSMM_allowed(ty);}
};

//	Attribute ResonanceStrengths class definition
class ResonanceStrengthsAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for ResonanceStrengths attribute
	constexpr static long X_DATA_SIZE = 610;
	ResonanceStrengthsAttrib():SpectrumAttr("ResonanceStrengths",
			Tango::DEV_DOUBLE, Tango::READ_WRITE, ResonanceStrengthsAttrib::X_DATA_SIZE) {}
	~ResonanceStrengthsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_ResonanceStrengths(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<MagnetType *>(dev))->write_ResonanceStrengths(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_ResonanceStrengths_allowed(ty);}
};

//	Attribute ResonanceStrengthsMSMM class definition
class ResonanceStrengthsMSMMAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for ResonanceStrengthsMSMM attribute
	constexpr static long X_DATA_SIZE = 4;
	ResonanceStrengthsMSMMAttrib():SpectrumAttr("ResonanceStrengthsMSMM",
			Tango::DEV_DOUBLE, Tango::READ, ResonanceStrengthsMSMMAttrib::X_DATA_SIZE) {}
	~ResonanceStrengthsMSMMAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_ResonanceStrengthsMSMM(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_ResonanceStrengthsMSMM_allowed(ty);}
};

//	Attribute MagnetNames class definition
class MagnetNamesAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for MagnetNames attribute
	constexpr static long X_DATA_SIZE = 610;
	MagnetNamesAttrib():SpectrumAttr("MagnetNames",
			Tango::DEV_STRING, Tango::READ, MagnetNamesAttrib::X_DATA_SIZE) {}
	~MagnetNamesAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_MagnetNames(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_MagnetNames_allowed(ty);}
};

//	Attribute MagnetStates class definition
class MagnetStatesAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for MagnetStates attribute
	constexpr static long X_DATA_SIZE = 620;
	MagnetStatesAttrib():SpectrumAttr("MagnetStates",
			Tango::DEV_STATE, Tango::READ, MagnetStatesAttrib::X_DATA_SIZE) {}
	~MagnetStatesAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_MagnetStates(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_MagnetStates_allowed(ty);}
};

//	Attribute RequireCycling class definition
class RequireCyclingAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for RequireCycling attribute
	constexpr static long X_DATA_SIZE = 620;
	RequireCyclingAttrib():SpectrumAttr("RequireCycling",
			Tango::DEV_BOOLEAN, Tango::READ, RequireCyclingAttrib::X_DATA_SIZE) {}
	~RequireCyclingAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_RequireCycling(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_RequireCycling_allowed(ty);}
};

//	Attribute FrozenMagnets class definition
class FrozenMagnetsAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for FrozenMagnets attribute
	constexpr static long X_DATA_SIZE = 620;
	FrozenMagnetsAttrib():SpectrumAttr("FrozenMagnets",
			Tango::DEV_STRING, Tango::READ, FrozenMagnetsAttrib::X_DATA_SIZE) {}
	~FrozenMagnetsAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_FrozenMagnets(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_FrozenMagnets_allowed(ty);}
};

//	Attribute IsFrozen class definition
class IsFrozenAttrib: public Tango::SpectrumAttr
{
public:
	// Constants for IsFrozen attribute
	constexpr static long X_DATA_SIZE = 620;
	IsFrozenAttrib():SpectrumAttr("IsFrozen",
			Tango::DEV_BOOLEAN, Tango::READ_WRITE, IsFrozenAttrib::X_DATA_SIZE) {}
	~IsFrozenAttrib() {}
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<MagnetType *>(dev))->read_IsFrozen(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<MagnetType *>(dev))->write_IsFrozen(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<MagnetType *>(dev))->is_IsFrozen_allowed(ty);}
};


//=========================================
//	Define classes for commands
//=========================================
//	Command Reset class definition
class ResetClass : public Tango::Command
{
public:
	ResetClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(cmd_name,in,out,in_desc,out_desc, level)	{}

	ResetClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(cmd_name,in,out)	{}
	~ResetClass() {}

	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<MagnetType *>(dev))->is_Reset_allowed(any);}
};

//	Command ResetResonanceStrengths class definition
class ResetResonanceStrengthsClass : public Tango::Command
{
public:
	ResetResonanceStrengthsClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(cmd_name,in,out,in_desc,out_desc, level)	{}

	ResetResonanceStrengthsClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(cmd_name,in,out)	{}
	~ResetResonanceStrengthsClass() {}

	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<MagnetType *>(dev))->is_ResetResonanceStrengths_allowed(any);}
};

//	Command SetpointCheck class definition
class SetpointCheckClass : public Tango::Command
{
public:
	SetpointCheckClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(cmd_name,in,out,in_desc,out_desc, level)	{}

	SetpointCheckClass(const char   *cmd_name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(cmd_name,in,out)	{}
	~SetpointCheckClass() {}

	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<MagnetType *>(dev))->is_SetpointCheck_allowed(any);}
};


/**
 *	The MagnetTypeClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  MagnetTypeClass : public Tango::DeviceClass
#else
class MagnetTypeClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(MagnetTypeClass::Additional DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	MagnetTypeClass::Additional DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
		//	Method prototypes
		static MagnetTypeClass *init(const char *);
		static MagnetTypeClass *instance();
		~MagnetTypeClass();
		Tango::DbDatum	get_class_property(std::string &);
		Tango::DbDatum	get_default_device_property(std::string &);
		Tango::DbDatum	get_default_class_property(std::string &);

	protected:
		MagnetTypeClass(std::string &);
		static MagnetTypeClass *_instance;
		void command_factory();
		void attribute_factory(std::vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		std::string get_cvstag();
		std::string get_cvsroot();

	private:
		void device_factory(TANGO_UNUSED(const Tango::DevVarStringArray *));
		void create_static_attribute_list(std::vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,std::vector<Tango::Attr *> &);
		std::vector<std::string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(std::vector<Tango::Attr *> &att_list, std::string attname);
};

}	//	End of namespace

#endif   //	MagnetType_H
